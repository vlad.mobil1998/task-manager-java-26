package integration;

import marker.IntegrationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.amster.tm.endpoint.ProjectDTO;
import ru.amster.tm.endpoint.ProjectEndpoint;
import ru.amster.tm.endpoint.SessionEndpoint;
import ru.amster.tm.service.WebServiceLocator;

@Ignore
@Category(IntegrationTest.class)
public class ProjectTest {

    private static ProjectEndpoint projectEndpoint;

    private static SessionEndpoint sessionEndpoint;

    private static String session, session1;

    @BeforeClass
    public static void init() {
        WebServiceLocator webServiceLocator = new WebServiceLocator();
        projectEndpoint = webServiceLocator.getProjectEndpoint();
        sessionEndpoint = webServiceLocator.getSessionEndpoint();

        session = sessionEndpoint.openSession("test", "test");
        session1 = sessionEndpoint.openSession("admin", "admin");

        projectEndpoint.createWithTwoParamProject(session, "demo");
        projectEndpoint.createWithTwoParamProject(session, "test");
        projectEndpoint.createWithTwoParamProject(session, "temp");

        projectEndpoint.createWithThreeParamProject(session1, "demo", "session1");
    }

    @Test
    @Category(IntegrationTest.class)
    public void findAndRemoveProjectTest() {
        ProjectDTO project = projectEndpoint.findProjectByName(session, "demo");
        Assert.assertEquals("demo", project.getName());
        Assert.assertNotEquals("session1", project.getDescription());

        ProjectDTO project1 = projectEndpoint.findProjectByName(session, "test");
        Assert.assertEquals(project1.getName(), "test");

        ProjectDTO project2 = projectEndpoint.findProjectByName(session1, "demo");
        Assert.assertEquals("demo", project2.getName());
        Assert.assertEquals("session1", project2.getDescription());

        projectEndpoint.removeProjectByName(session, "demo");
        ProjectDTO project5 = projectEndpoint.findProjectByName(session1, "demo");
        Assert.assertEquals("demo", project5.getName());
        Assert.assertEquals("session1", project5.getDescription());
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void projectCreateTestException() {
        projectEndpoint.createWithTwoParamProject("adsedqwd2132dad/sadasda/sdas89yhuhqd", "ds");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void projectShowByNameTestException() {
        projectEndpoint.findProjectByName("adsedqwd2132dad/sadasda/sdas89yhuhqd", "demo");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void projectRemoveByNameTestException() {
        projectEndpoint.removeProjectByName("adsedqwd2132dad/sadasda/sdas89yhuhqd", "demo");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void projectCreateTestNullException() {
        projectEndpoint.createWithTwoParamProject(session, null);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void projectShowByNameTestNullException() {
        projectEndpoint.findProjectByName(session, null);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void projectRemoveByNameTestNullException() {
        projectEndpoint.removeProjectByName(session, null);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void projectCreateTestEmptyException() {
        projectEndpoint.createWithTwoParamProject(session, "");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void projectShowByNameTestEmptyException() {
        projectEndpoint.findProjectByName(session, "");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void projectRemoveByNameTestEmptyException() {
        projectEndpoint.removeProjectByName(session, "");
    }

    @AfterClass
    public static void exit() {
        projectEndpoint.removeProjectByName(session, "test");
        projectEndpoint.removeProjectByName(session, "temp");
        projectEndpoint.removeProjectByName(session1, "demo");
        sessionEndpoint.closeSession(session1);
        sessionEndpoint.closeSession(session);
    }

}