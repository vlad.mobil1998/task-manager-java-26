package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public final class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("ERROR! Role is empty...");
    }

}