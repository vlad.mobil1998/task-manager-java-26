package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.endpoint.*;

public interface IServiceLocator {

    @NotNull String getSession();

    void setSession(String session);

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    ICommandService getCommandService();

    void setCommandService(ICommandService commandService);

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

}