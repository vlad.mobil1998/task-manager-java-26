package ru.amster.tm.command.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.endpoint.UserDTO;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.List;

public final class AllUserShowCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "view-all-users";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show All Users";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW ALL USER]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        @NotNull final AdminUserEndpoint adminUserEndpoint = webServiceLocator.getAdminUserEndpoint();
        @NotNull final List<UserDTO> users = adminUserEndpoint.findAllUser(webServiceLocator.getSession());
        for (@NotNull final UserDTO item : users) {
            System.out.println("[LOGIN]");
            System.out.println(item.getLogin());
            if (item.getEmail() != null && !item.getEmail().isEmpty()) {
                System.out.println("[EMAIL]");
                System.out.println(item.getEmail());
            }
            if (item.getFistName() != null && !item.getFistName().isEmpty()) {
                System.out.println("[FIST NAME]");
                System.out.println(item.getFistName());
            }
            if (item.getMiddleName() != null && !item.getMiddleName().isEmpty()) {
                System.out.println("[MIDDLE NAME]");
                System.out.println(item.getMiddleName());
            }
            if (item.getLastName() != null && !item.getLastName().isEmpty()) {
                System.out.println("[LAST NAME]");
                System.out.println(item.getLastName());
            }
            System.out.println("[OK]");
        }
    }

}