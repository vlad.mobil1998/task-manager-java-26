package ru.amster.tm.command.admin.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.admin.data.AbstractDataCommand;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.endpoint.Domain;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

public final class DataJsonSaveCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-json-save";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Save data from json file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA JSON SAVE]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        @NotNull final File file = new File(FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final AdminUserEndpoint adminUserEndpoint = webServiceLocator.getAdminUserEndpoint();
        @NotNull final Domain domain = adminUserEndpoint.export(webServiceLocator.getSession());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[OK]");
    }

}