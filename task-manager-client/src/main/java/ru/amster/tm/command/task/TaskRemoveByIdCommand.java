package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.TaskEndpoint;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-re-id";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove task by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        System.out.println("ENTER ID");
        @Nullable final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final TaskEndpoint taskEndpoint = webServiceLocator.getTaskEndpoint();
        taskEndpoint.removeTaskById(webServiceLocator.getSession(), id);
        System.out.println("[OK]");
    }

}