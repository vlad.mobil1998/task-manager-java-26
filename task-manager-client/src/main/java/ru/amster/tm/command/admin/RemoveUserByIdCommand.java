package ru.amster.tm.command.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class RemoveUserByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "re-user-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Remove user by id";
    }

    @Override
    public void execute() {
        System.out.println("[DELETED USER BY ID]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        System.out.println("ENTER ID");
        @Nullable final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyLoginException();

        @NotNull final AdminUserEndpoint adminUserEndpoint = webServiceLocator.getAdminUserEndpoint();
        adminUserEndpoint.removeUserById(webServiceLocator.getSession(), id);
        System.out.println("[OK]");
    }

}