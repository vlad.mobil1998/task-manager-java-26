package ru.amster.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.command.AbstractCommand;

import java.util.Collection;

public final class SystemHelpCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "help";
    }

    @Override
    @NotNull
    public String arg() {
        return "-h";
    }

    @Override
    @NotNull
    public String description() {
        return " - Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = webServiceLocator.getCommandService().getCommands();
        for (AbstractCommand command : commands) {
            if (command.arg() == null) System.out.println(command.name() + command.description());
            else System.out.println(command.name() + " " + command.arg() + command.description());
        }
    }

}