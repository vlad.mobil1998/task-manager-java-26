package endpoint;

import org.junit.*;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.dto.UserDTO;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.endpoint.ProjectEndpoint;
import ru.amster.tm.endpoint.SessionEndpoint;
import ru.amster.tm.endpoint.TaskEndpoint;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Session;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.service.ServiceLocator;

@Ignore
public class EndpointTest {

    private static IServiceLocator serviceLocator;

    private static SessionEndpoint sessionEndpoint;

    private static ProjectEndpoint projectEndpoint;

    private static UserEndpoint userEndpoint;

    private static TaskEndpoint taskEndpoint;

    private static String sessionTest;

    private static String sessionTest1;

    private static UserDTO user;

    private static UserDTO user1;

    @BeforeClass
    public static void init() throws Exception {
        serviceLocator = new ServiceLocator();
        serviceLocator.getPropertyServer().init();
        serviceLocator.getEntityManagerService().initTest();
        sessionEndpoint = new SessionEndpoint(serviceLocator);
        projectEndpoint = new ProjectEndpoint(serviceLocator);
        taskEndpoint = new TaskEndpoint(serviceLocator);
        userEndpoint = new UserEndpoint(serviceLocator);
        userEndpoint.createWithThreeParamUser("test", "test", Role.USER);
        userEndpoint.createWithThreeParamUser("admin", "admin", Role.USER);
        sessionTest = sessionEndpoint.openSession("test", "test");
        sessionTest1 = sessionEndpoint.openSession("admin", "admin");
        user = userEndpoint.findUser(sessionTest);
        user1 = userEndpoint.findUser(sessionTest1);
    }

    @Test
    public void login()  {
        Session session;
        session = serviceLocator.getSessionService().validate(sessionTest1);
        Assert.assertEquals(session.getUser().getId(), user1.getId());
    }

    @Test(expected = AccessDeniedException.class)
    public void logout()  {
        projectEndpoint.createWithTwoParamProject(sessionTest, "test");
        sessionEndpoint.closeSession(sessionTest);
        projectEndpoint.findProjectByName(sessionTest, "test");
    }

    @Test(expected = EmptyProjectException.class)
    public void projectCreate()  {
        projectEndpoint.createWithTwoParamProject(sessionTest1, "demo");
        Session session = serviceLocator.getSessionService().validate(sessionTest1);
        Project project = serviceLocator.getProjectService()
                .findOneByName(session.getUser().getId(),"demo");

        Assert.assertEquals("demo", project.getName());
        Assert.assertEquals(session.getUser().getId(), project.getUser().getId());

        sessionTest = sessionEndpoint.openSession("test", "test");
        session = serviceLocator.getSessionService().validate(sessionTest);
        serviceLocator.getProjectService().findOneByName(session.getUser().getId(),"demo");
    }

    @Test
    public void taskCreateAndFindTest() {
        sessionTest = sessionEndpoint.openSession("test", "test");
        projectEndpoint.createWithTwoParamProject(sessionTest, "demo");
        projectEndpoint.createWithTwoParamProject(sessionTest, "demka");
        taskEndpoint.createWithTwoParamTask(sessionTest, "demoTask", "demo");
        taskEndpoint.findTaskByName(sessionTest, "demoTask");
    }

    @AfterClass
    public static void exit() {
        serviceLocator.getUserService().clear();
    }

}