package service.project;

import marker.UnitCategory;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.service.ServiceLocator;

@Ignore
public class ProjectServiceTest {

    private static IServiceLocator serviceLocator;

    private static IProjectService projectService;

    private static User user;

    private static User user1;

    private static Project project;

    private static Project project1;

    @BeforeClass
    public static void addProjectData() throws Exception {
        serviceLocator = new ServiceLocator();
        serviceLocator.getPropertyServer().init();
        serviceLocator.getEntityManagerService().initTest();
        projectService = serviceLocator.getProjectService();
        IUserService userService = serviceLocator.getUserService();
        userService.create("test", "test", Role.USER);
        userService.create("temp", "temp", Role.USER);
        user = userService.findByLogin("test");
        user1 = userService.findByLogin("temp");
        projectService.create(user.getId(), "test");
        projectService.create(user1.getId(), "temp");
        project = projectService.findOneByName(user.getId(), "test");
        project1 = projectService.findOneByName(user1.getId(), "temp");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void createProjectTestExceptionEmpty() {
        projectService.create("", "ds", "sd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void createProjectTestExceptionNull() {
        projectService.create(null, "sd", "ad");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void createProjectTestExceptionEmpty1() {
        projectService.create("1", "", "ds");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void createProjectTestExceptionNull1() {
        projectService.create("1", null, "sds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionEmpty() {
        projectService.findOneByName("", "ds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionNull() {
        projectService.findOneByName(null, "sd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionEmpty1() {
        projectService.findOneByName("sd", "");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionNull1() {
        projectService.findOneByName("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        Project projectTest = projectService.findOneByName(user.getId(), "test");
        Assert.assertEquals(projectTest.getId(), project.getId());

        Project projectTest1 = projectService.findOneById(user1.getId(), project1.getId());
        Assert.assertEquals(projectTest1.getId(), project1.getId());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionEmpty2() {
        projectService.findOneById("", "sd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionNull1() {
        projectService.findOneById(null, "ds");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionEmpty() {
        projectService.findOneById("sds", "");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionNull() {
        projectService.findOneById("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTest() {
        projectService.removeOneById(user1.getId(), project1.getId());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionEmpty2() {
        projectService.removeOneById("", "sd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionNull1() {
        projectService.removeOneById(null, "ds");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionEmpty() {
        projectService.removeOneById("sds", "");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionNull() {
        projectService.removeOneById("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByNameTest() {
        projectService.create(user1.getId(), "temp");
        projectService.removeOneByName(user1.getId(), "temp");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionEmpty() {
        projectService.removeOneByName("", "ds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionNull() {
        projectService.removeOneByName(null, "sd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionEmpty1() {
        projectService.removeOneByName("sd", "");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionNull1() {
        projectService.removeOneByName("ds", null);
    }

    @Test(expected = EmptyProjectException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestException() {
        projectService.updateProjectById("1", "213", "ds", "ds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestExceptionEmpty() {
        projectService.updateProjectById("", "sd", "dsa", "saq");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestExceptionNull1() {
        projectService.updateProjectById(null, "ds", "we", "re");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestExceptionEmpty1() {
        projectService.updateProjectById("sds", "", "we", "qwe");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestExceptionNull() {
        projectService.updateProjectById("ds", null, "esa", "asd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestExceptionEmpty2() {
        projectService.updateProjectById("sds", "sd", "", "qwe");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestExceptionNull2() {
        projectService.updateProjectById("ds", "sda", null, "asd");
    }

    @AfterClass
    public static void exit() {
        serviceLocator.getUserService().clear();
    }

}