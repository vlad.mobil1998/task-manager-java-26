package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IEntityManagerService {

    void initTest();

    void init();

    @NotNull
    EntityManager getEntityManager();

}