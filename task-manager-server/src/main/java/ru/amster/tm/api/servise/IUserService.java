package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.User;

import java.util.List;

public interface IUserService {

    void create(@Nullable String login, @Nullable String password, @Nullable Role role);

    void create(@Nullable String login, @Nullable String password, @Nullable String email);

    void create(@Nullable String login, @Nullable String password, @Nullable String email, @Nullable Role role);

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    void removeUser(@Nullable User user);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    void updateEmail(@Nullable String id, @Nullable String email);

    void updateFirstName(@Nullable String id, @Nullable String firstName);

    void updateLastName(@Nullable String id, @Nullable String lastName);

    void updateMiddleName(@Nullable String id, @Nullable String middleName);

    void updatePassword(@Nullable String id, @Nullable String password);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    void load(@NotNull List<User> users);

    @Nullable
    List<User> export();

    @Nullable
    List<User> findAll();

    void add(@Nullable User user);

    void clear();

}