package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    @NotNull
    List<Task> findAll(@Nullable String userId);

    void removeAll(@Nullable String userId);

    @Nullable
    Task findOneById(@Nullable String userId, @NotNull String id);

    @Nullable
    Task findOneByName(@Nullable String userId, @NotNull String name);

    @NotNull
    void removeOneByName(@Nullable String userId, @NotNull String name);

    @NotNull
    void removeOneById(@Nullable String userId, @NotNull String id);

    @NotNull
    Long numberOfAllTasks(@Nullable String userId);

    @NotNull List<Task> getEntity();

    void removeAll();

    void remove(@NotNull Task task);

    void merge(@NotNull Task record);

}