package ru.amster.tm.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Session;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Data
public final class SessionDTO {

    private String id = UUID.randomUUID().toString();

    private String userId;

    @NotNull
    private Long timestamp;

    @Nullable
    private String signature;

    public SessionDTO(Session session) {
        this.id = session.getId();
        this.userId = session.getUser().getId();
        this.timestamp = session.getTimestamp();
        this.signature = session.getSignature();
    }

}