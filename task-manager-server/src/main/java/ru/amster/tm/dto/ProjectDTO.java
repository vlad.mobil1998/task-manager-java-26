package ru.amster.tm.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Project;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Data
public final class ProjectDTO {

    @NotNull
    private String id;

    @NotNull
    private String userId;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Date dataBegin;

    @NotNull
    private Date dataEnd;

    public ProjectDTO(@Nullable final Project project) {
        if (project == null) return;
        id = project.getId();
        if (project.getUser() != null) userId = project.getUser().getId();
        description = project.getDescription();
        dataBegin = project.getDataBegin();
        dataEnd = project.getDataEnd();
    }

}