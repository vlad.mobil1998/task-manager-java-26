package ru.amster.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    @NotNull
    private final EntityManager entityManager;

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Project> criteriaQuery
                = criteriaBuilder.createQuery(Project.class);
        final Root<Project> root = criteriaQuery.from(Project.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "user");

        final Predicate predicate = criteriaBuilder.equal(pathUserId, parameterUserId);
        criteriaQuery.where(predicate);

        final TypedQuery<Project> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Project> criteriaDelete
                = criteriaBuilder.createCriteriaDelete(Project.class);
        final Root<Project> root = criteriaDelete.from(Project.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "user");

        final Predicate predicate
                = criteriaBuilder.equal(pathUserId, parameterUserId);
        criteriaDelete.where(predicate);

        final Query query = entityManager.createQuery(criteriaDelete);
        query.setParameter("user", userId);
        query.executeUpdate();
    }

    @Nullable
    @Override
    public Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Project> criteriaQuery
                = criteriaBuilder.createQuery(Project.class);
        final Root<Project> root = criteriaQuery.from(Project.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "user");
        final Path<String> pathName = root.get("id");
        final ParameterExpression<String> parameterName
                = criteriaBuilder.parameter(String.class, "id");

        final Predicate predicate;
        predicate = criteriaBuilder.and(
                criteriaBuilder.equal(pathUserId, parameterUserId),
                criteriaBuilder.equal(pathName, parameterName)
        );
        criteriaQuery.where(predicate);

        final TypedQuery<Project> typedQuery
                = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("user", userId);
        typedQuery.setParameter("id", id);
        return typedQuery.getSingleResult();
    }

    @Nullable
    @Override
    public Project findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Project> criteriaQuery
                = criteriaBuilder.createQuery(Project.class);
        final Root<Project> root = criteriaQuery.from(Project.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "user");
        final Path<String> pathName = root.get("name");
        final ParameterExpression<String> parameterName
                = criteriaBuilder.parameter(String.class, "name");

        final Predicate predicate;
        predicate = criteriaBuilder.and(
                criteriaBuilder.equal(pathUserId, parameterUserId),
                criteriaBuilder.equal(pathName, parameterName)
        );
        criteriaQuery.where(predicate);

        final TypedQuery<Project> typedQuery
                = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("user", userId);
        typedQuery.setParameter("name", name);
        return typedQuery.getSingleResult();
    }

    @NotNull
    @Override
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Project> criteriaDelete
                = criteriaBuilder.createCriteriaDelete(Project.class);
        final Root<Project> root = criteriaDelete.from(Project.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "user");
        final Path<String> pathName = root.get("name");
        final ParameterExpression<String> parameterName
                = criteriaBuilder.parameter(String.class, "name");

        final Predicate predicate;
        predicate = criteriaBuilder.and(
                criteriaBuilder.equal(pathUserId, parameterUserId),
                criteriaBuilder.equal(pathName, parameterName)
        );
        criteriaDelete.where(predicate);

        final Query query = entityManager.createQuery(criteriaDelete);
        query.setParameter("user", userId);
        query.setParameter("name", name);
        query.executeUpdate();
    }

    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Project> criteriaDelete
                = criteriaBuilder.createCriteriaDelete(Project.class);
        final Root<Project> root = criteriaDelete.from(Project.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "user");
        final Path<String> pathName = root.get("id");
        final ParameterExpression<String> parameterName
                = criteriaBuilder.parameter(String.class, "id");

        final Predicate predicate;
        predicate = criteriaBuilder.and(
                criteriaBuilder.equal(pathUserId, parameterUserId),
                criteriaBuilder.equal(pathName, parameterName)
        );
        criteriaDelete.where(predicate);

        final Query query = entityManager.createQuery(criteriaDelete);
        query.setParameter("user", userId);
        query.setParameter("id", id);
        query.executeUpdate();
    }

    @NotNull
    @Override
    public Long numberOfAllProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Long> criteriaQuery
                = criteriaBuilder.createQuery(Long.class);
        final Root<Project> root = criteriaQuery.from(Project.class);
        criteriaQuery.select(criteriaBuilder.count(root));

        final Path<String> pathId = root.get("user").get("id");
        final ParameterExpression<String> parameterId
                = criteriaBuilder.parameter(String.class, "user");
        final Predicate predicateById
                = criteriaBuilder.equal(pathId, parameterId);
        criteriaQuery.where(predicateById);

        final TypedQuery<Long> typedQuery
                = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("user", userId);
        return typedQuery.getSingleResult();
    }

    @NotNull
    @Override
    public List<Project> getEntity() {
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Project> criteriaQuery
                = criteriaBuilder.createQuery(Project.class);
        criteriaQuery.from(Project.class);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public void remove(@Nullable final Project project) {
        if (project == null) throw new EmptyProjectException();

        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Project> criteriaDelete
                = criteriaBuilder.createCriteriaDelete(Project.class);
        final Root<Project> root = criteriaDelete.from(Project.class);

        final Path<Project> pathProject = root.get("project");
        final CriteriaDelete<Project> parameterId
                = criteriaBuilder.createCriteriaDelete(Project.class);

        final Predicate predicateId
                = criteriaBuilder.equal(pathProject, parameterId);
        criteriaDelete.where(predicateId);

        final Query query = entityManager.createQuery(criteriaDelete);
        query.setParameter("project", project);
        query.executeUpdate();
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) throw new EmptyProjectException();
        entityManager.merge(project);
    }

    @Override
    public void removeAll() {
        List<Project> projects = getEntity();
        for (Project project: projects) {
            entityManager.remove(project);
        }
    }

}