package ru.amster.tm.exception.system;

import ru.amster.tm.exception.AbstractException;

public class ProjectCreated extends AbstractException {

    public ProjectCreated() {
        super("ERROR! Project created...");
    }
}
