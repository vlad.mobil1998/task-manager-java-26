package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public final class EmptyRecordException extends AbstractException {

    public EmptyRecordException() {
        super("Error! Record is empty...");
    }

}