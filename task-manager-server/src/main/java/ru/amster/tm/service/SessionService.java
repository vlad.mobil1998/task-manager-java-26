package ru.amster.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.cj.exceptions.PasswordExpiredException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.ISessionRepository;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.dto.SessionDTO;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyEntityException;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptySignatureException;
import ru.amster.tm.exception.empty.EmptyUserException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.repository.SessionRepository;
import ru.amster.tm.util.EncryptUtil;
import ru.amster.tm.util.HashUtil;
import ru.amster.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class SessionService implements ISessionService {

    @NotNull
    private IServiceLocator serviceLocator;

    public SessionService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void close(@NotNull final String sessionSecret) {
        Session session = validate(sessionSecret);
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ISessionRepository sessionRepository = new SessionRepository(em);
            sessionRepository.removeByUserId(session.getUser().getId());
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Session): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        SessionDTO sessionDTO = new SessionDTO(session);
        @Nullable final String signature = SignatureUtil.sign(sessionDTO);
        if (signature == null) throw new EmptySignatureException();
        session.setSignature(signature);
        return session;
    }

    @Override
    @NotNull
    public Session validate(@Nullable final String sessionSecret) {
        if (sessionSecret == null || sessionSecret.isEmpty()) throw new  AccessDeniedException();
        SessionDTO sessionDTO = null;
        try {
            sessionDTO = decryptSession(sessionSecret);
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        }

        if (sessionDTO == null) throw new AccessDeniedException();

        if (sessionDTO.getSignature() == null
                || sessionDTO.getSignature().isEmpty()) throw new AccessDeniedException();

        if (sessionDTO.getUserId() == null
                || sessionDTO.getUserId().isEmpty()) throw new AccessDeniedException();

        if (sessionDTO.getTimestamp() == null) throw new AccessDeniedException();

        if(System.currentTimeMillis() - sessionDTO.getTimestamp() > 300000) {
            signOutByUserId(sessionDTO.getId());
            close(sessionSecret);
            throw new AccessDeniedException();
        }

        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable User user = userService.findById(sessionDTO.getUserId());
        if (user == null) throw new EmptyUserException();

        if (user.getLocked()) throw new AccessDeniedException();

        Session session = new Session(user);
        session.setId(sessionDTO.getId());
        session.setTimestamp(sessionDTO.getTimestamp());
        session.setSignature(sessionDTO.getSignature());
        @Nullable final Session sessionTemp = session.clone();
        if (sessionTemp == null) throw new AccessDeniedException();

        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(sessionTemp).getSignature();
        @NotNull final Boolean chek = signatureSource.equals(signatureTarget);
        if (!chek) throw new AccessDeniedException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ISessionRepository sessionRepository = new SessionRepository(em);
            if (!sessionRepository.contains(sessionDTO.getId())) throw new AccessDeniedException();
        } catch (AccessDeniedException e) {
            throw new AccessDeniedException();
        } catch (RuntimeException e) {
            System.out.println("Message exception (Session): " + e.getMessage());
        } finally {
            em.close();
        }
        return session;
    }

    @Override
    public Session validate(
            @NotNull final String sessionSecret,
            @Nullable final Role role
    ) {
        if (role == null) throw new AccessDeniedException();

        @Nullable Session session = null;

        try {
            session = validate(sessionSecret);
        } catch (Exception e) {
            System.out.println("Message exception (Session): " + e.getMessage());
        }
        @NotNull final String userId = session.getUser().getId();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final User user = userService.findById(userId);

        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
        return session;
    }

    @Override
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || login.isEmpty()) return false;

        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable User user;

        try {
            user = userService.findByLogin(login);
        } catch (EmptyUserException e) {
            return false;
        } catch (RuntimeException e) {
            System.out.println("Message exception (Session): " + e.getMessage());
            return false;
        }
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    public String open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new PasswordExpiredException();

        final boolean check = checkDataAccess(login, password);
        if (!check) return null;

        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user;
        try {
            user = userService.findByLogin(login);
        } catch (EmptyUserException e) {
            throw new EmptyUserException();
        }
        if (user.getLocked()) throw new AccessDeniedException();

        @NotNull final Session session = new Session(user);
        session.setTimestamp(System.currentTimeMillis());
        sign(session);

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ISessionRepository sessionRepository = new SessionRepository(em);
            sessionRepository.merge(session);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Session): " + e.getMessage());
        } finally {
            em.close();
        }
        SessionDTO sessionDTO = new SessionDTO(session);

        try {
            return cryptSession(sessionDTO);
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        }
        return null;
    }

    private String cryptSession(@NotNull final SessionDTO session) throws  Exception {
        @NotNull final String key = serviceLocator.getPropertyServer().getSecretKey();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        return EncryptUtil.encrypt(json, key);
    }

    private SessionDTO decryptSession(@Nullable final String cryptSession) throws Exception {
        if (cryptSession == null || cryptSession.isEmpty()) throw new EmptyEntityException();

        @NotNull final String key = serviceLocator.getPropertyServer().getSecretKey();
        @NotNull final String json = EncryptUtil.decrypt(cryptSession, key);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final SessionDTO session = objectMapper.readValue(json, SessionDTO.class);
        return session;
    }

    @Override
    public void signOutByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;

        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user;
        try {
            user = userService.findByLogin(login);
        } catch (EmptyUserException e) {
            throw new EmptyUserException();
        }
        if (user == null) return;
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final String userId = user.getId();
        try {
            em.getTransaction().begin();
            ISessionRepository sessionRepository = new SessionRepository(em);
            sessionRepository.removeByUserId(userId);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Session): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void signOutByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ISessionRepository sessionRepository = new SessionRepository(em);
            sessionRepository.removeByUserId(userId);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Session): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void load(@NotNull final List<Session> sessions) {
        clear();
        for (@NotNull Session session: sessions) {
            add(session);
        }
    }

    @Override
    @NotNull
    public List<Session> export() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ISessionRepository repository = new SessionRepository(em);
            List<Session> sessions = repository.getEntity();
            return sessions;
        } catch (RuntimeException e) {
            System.out.println("Message exception (Session): " + e.getMessage());
            return null;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ISessionRepository repository = new SessionRepository(em);
            List<Session> sessions = repository.getEntity();
            return sessions;
        } catch (RuntimeException e) {
            System.out.println("Message exception (Session): " + e.getMessage());
            return null;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable final Session session) {
        if (session == null) throw new EmptyEntityException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ISessionRepository repository = new SessionRepository(em);
            repository.merge(session);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Session): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void clear() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ISessionRepository repository = new SessionRepository(em);
            repository.removeAll();
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

}