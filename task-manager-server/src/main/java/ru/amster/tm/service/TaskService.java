package ru.amster.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Task;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.*;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@NoArgsConstructor
public final class TaskService implements ITaskService {

    @NotNull
    private IServiceLocator serviceLocator;

    public TaskService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Project project;
        @Nullable User user;
        try {
            project = serviceLocator.getProjectService().findOneById(userId, projectId);
            user = serviceLocator.getUserService().findById(userId);
        } catch (EmptyProjectException e) {
            throw new EmptyProjectException();
        } catch (EmptyUserException e) {
            throw new EmptyUserException();
        } catch (RuntimeException e) {
            System.out.println("Message exception (Task): " + e.getMessage());
            return;
        }
        @NotNull final Task task = new Task(name, project, user);
        add(task);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (projectId == null || projectId.isEmpty())
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;

        Project project;
        User user;
        try {
            project = serviceLocator.getProjectService().findOneById(userId, projectId);
            user = serviceLocator.getUserService().findById(userId);
        } catch (EmptyProjectException e) {
            throw new EmptyProjectException();
        } catch (EmptyUserException e) {
            throw new EmptyUserException();
        } catch (RuntimeException e) {
            System.out.println("Message exception (Task): " + e.getMessage());
            return;
        }
        @NotNull final Task task = new Task(name, project, user);
        task.setDescription(description);
        add(task);
    }

    @Nullable
    @Override
    public Task findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        Task task;
        try {
            em.getTransaction().begin();
            ITaskRepository taskRepository = new TaskRepository(em);
            task = taskRepository.findOneByName(userId, name);
            return task;
        } catch (NoResultException e) {
            throw new EmptyTaskException();
        } catch (RuntimeException e) {
            System.out.println("Message exception (Task): " + e.getMessage());
            return null;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        ITaskRepository taskRepository = new TaskRepository(em);
        Task task;
        try {
            task = taskRepository.findOneById(userId, id);
            return task;
        } catch (NoResultException e) {
            throw new EmptyTaskException();
        } catch (RuntimeException e) {
            System.out.println("Message exception (Task): " + e.getMessage());
            return null;
        }finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ITaskRepository taskRepository = new TaskRepository(em);
            taskRepository.removeOneByName(userId, name);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Task): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ITaskRepository taskRepository = new TaskRepository(em);
            taskRepository.removeOneById(userId, id);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Task): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public void updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        @Nullable final Task task;
        try {
            em.getTransaction().begin();
            ITaskRepository taskRepository = new TaskRepository(em);
            task = taskRepository.findOneById(userId, id);
            task.setId(id);
            task.setName(name);
            task.setDescription(description);
            em.merge(task);
            em.getTransaction().commit();
        } catch (NoResultException e) {
            em.getTransaction().rollback();
            throw new EmptyTaskException();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Task): " + e.getMessage());
            return;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public Long numberOfAllTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        Long count;
        try {
            em.getTransaction().begin();
            ITaskRepository taskRepository = new TaskRepository(em);
            count = taskRepository.numberOfAllTasks(userId);
            return count;
        } catch (RuntimeException e) {
            System.out.println("Message exception (Task): " + e.getMessage());
            return null;
        }
        finally {
            em.close();
        }
    }

    @Override
    public void load(@NotNull final List<Task> tasks) {
        clear();
        for (@NotNull Task task: tasks) {
            add(task);
        }
    }

    @Override
    @Nullable
    public List<Task> export() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ITaskRepository repository = new TaskRepository(em);
            List<Task> tasks = repository.getEntity();
            return tasks;
        } catch (RuntimeException e) {
            System.out.println("Message exception (Task): " + e.getMessage());
            return null;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ITaskRepository repository = new TaskRepository(em);
            List<Task> tasks = repository.getEntity();
            return tasks;
        } catch (RuntimeException e) {
            System.out.println("Message exception (Task): " + e.getMessage());
            return null;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable final Task task) {
        if (task == null) throw new EmptyEntityException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ITaskRepository repository = new TaskRepository(em);
            repository.merge(task);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Task): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void clear() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            ITaskRepository repository = new TaskRepository(em);
            repository.removeAll();
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Task): " + e.getMessage());
        }finally {
            em.close();
        }
    }

}