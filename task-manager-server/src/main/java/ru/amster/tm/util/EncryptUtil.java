package ru.amster.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public final class EncryptUtil {

    private static SecretKeySpec secretKey;

    private static byte[] key;

    private static void setKey(@NotNull final String myKey) throws Exception {
        @Nullable MessageDigest sha;
        key = myKey.getBytes("UTF-8");
        sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16);
        secretKey = new SecretKeySpec(key, "AES");
    }

    @NotNull
    public static String encrypt(@NotNull String json, @NotNull String myKey) throws Exception {
        setKey(myKey);
        @NotNull final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        @NotNull final byte[] data = cipher.doFinal(json.getBytes("UTF-8"));
        return Base64.getEncoder().encodeToString(data);
    }

    @NotNull
    public static String decrypt(@NotNull String json, @NotNull String myKey) throws Exception {
        setKey(myKey);
        @NotNull final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        @NotNull final byte[] data = Base64.getDecoder().decode(json);
        return new String(cipher.doFinal(data));
    }

}